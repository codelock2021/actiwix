-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 23, 2022 at 05:56 AM
-- Server version: 10.5.15-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u402017191_actiwix`
--

-- --------------------------------------------------------

--
-- Table structure for table `  font_family`
--

CREATE TABLE `  font_family` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `  font_family`
--

INSERT INTO `  font_family` (`id`, `name`) VALUES
(1, 'Lato'),
(2, 'Roboto'),
(3, 'Josefin Sans'),
(4, 'Lobster'),
(5, 'Open sans'),
(6, 'Poiret One'),
(7, 'Dancing Script'),
(8, 'Bangers'),
(9, 'Playfair Display'),
(10, 'Chewy'),
(11, 'Quicksand'),
(12, 'Great Vibes'),
(13, 'Righteous'),
(14, 'Crafty Girls'),
(15, 'Mystery Quest'),
(16, 'Serif'),
(17, 'Helvetica'),
(18, 'Verdana'),
(19, 'Montserrat'),
(20, 'Oswald'),
(21, 'Unica One'),
(22, 'Muli'),
(23, 'Courier'),
(24, 'Raleway'),
(25, 'Carter One'),
(26, 'Varela Round');

-- --------------------------------------------------------

--
-- Table structure for table `login_user`
--

CREATE TABLE `login_user` (
  `id` int(11) NOT NULL,
  `login_user_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('0','1') NOT NULL,
  `store` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login_user`
--

INSERT INTO `login_user` (`id`, `login_user_id`, `email`, `user_name`, `password`, `role`, `store`, `status`, `created_at`, `updated_at`) VALUES
(1, 101, 'codelock2021@gmail.com', 'codelock', '202cb962ac59075b964b07152d234b70', '1', 'managedashboard.myshopify.com', 1, '2021-03-19 05:06:14', '2021-03-19 05:06:29');

-- --------------------------------------------------------

--
-- Table structure for table `store_setting`
--

CREATE TABLE `store_setting` (
  `store_setting_id` int(11) NOT NULL,
  `shop_user_id` int(11) NOT NULL,
  `font_family` varchar(255) NOT NULL,
  `product_title_css` varchar(255) NOT NULL,
  `original_pirce_css` varchar(255) NOT NULL,
  `discount_pirce_css` varchar(255) NOT NULL,
  `image_css` varchar(255) NOT NULL,
  `not_at_text` varchar(255) NOT NULL,
  `not_at_css` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_setting`
--

INSERT INTO `store_setting` (`store_setting_id`, `shop_user_id`, `font_family`, `product_title_css`, `original_pirce_css`, `discount_pirce_css`, `image_css`, `not_at_text`, `not_at_css`, `created_at`, `updated_at`) VALUES
(1, 1, '', 'a:2:{s:5:\"color\";s:7:\"#442020\";s:9:\"font-size\";s:2:\"14\";}', 'a:2:{s:5:\"color\";s:7:\"#af5d5d\";s:9:\"font-size\";s:2:\"13\";}', 'a:2:{s:5:\"color\";s:7:\"#b74949\";s:9:\"font-size\";s:3:\"120\";}', 'a:2:{s:5:\"width\";s:2:\"65\";s:6:\"height\";s:2:\"95\";}', 'none of above', 'a:2:{s:5:\"color\";s:7:\"#7d3333\";s:9:\"font-size\";s:2:\"11\";}', '2018-09-26 05:44:19', '2018-09-26 05:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_shops`
--

CREATE TABLE `user_shops` (
  `store_user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive',
  `application_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=enable,0=disable',
  `application_language` enum('en','es','he_IL','fil','fr','ru','sp','pt') DEFAULT 'en',
  `shop_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_demand_accept` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1:aproved; 0:not approved',
  `charge` float NOT NULL DEFAULT 0,
  `price_id` varchar(255) DEFAULT NULL,
  `invoice_on` date DEFAULT NULL,
  `operated_on` date DEFAULT NULL,
  `test_finals_on` date DEFAULT NULL,
  `removed_on` date DEFAULT NULL,
  `store_name` varchar(255) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `store_idea` varchar(25) NOT NULL,
  `price_pattern` varchar(60) NOT NULL,
  `cash` varchar(25) NOT NULL,
  `store_holder` varchar(255) NOT NULL,
  `address11` varchar(255) NOT NULL,
  `address22` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `division` varchar(50) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `america_timezone` varchar(50) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `feedback_status` enum('1','0') NOT NULL DEFAULT '0',
  `block_status` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `uninstall_on` timestamp NULL DEFAULT NULL,
  `install_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_shops`
--

INSERT INTO `user_shops` (`store_user_id`, `email`, `status`, `application_status`, `application_language`, `shop_name`, `password`, `is_demand_accept`, `charge`, `price_id`, `invoice_on`, `operated_on`, `test_finals_on`, `removed_on`, `store_name`, `api_key`, `store_idea`, `price_pattern`, `cash`, `store_holder`, `address11`, `address22`, `city`, `country_name`, `mobile_no`, `division`, `zip`, `timezone`, `america_timezone`, `domain`, `feedback_status`, `block_status`, `created_at`, `updated_at`, `uninstall_on`, `install_date`) VALUES
(11, '', '1', '0', 'en', 'clsapps.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'clsapps.myshopify.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '2022-07-19 12:31:32', '2022-07-19 12:31:32', NULL, NULL),
(12, '', '1', '0', 'en', 'dashboardmanage.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'dashboardmanage.myshopify.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '2022-07-22 03:55:14', '2022-07-22 03:55:14', NULL, NULL),
(13, '', '1', '0', 'en', 'cls-rewriter.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'cls-rewriter.myshopify.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '2022-07-22 04:03:41', '2022-07-22 04:03:41', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_user`
--
ALTER TABLE `login_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_setting`
--
ALTER TABLE `store_setting`
  ADD PRIMARY KEY (`store_setting_id`),
  ADD UNIQUE KEY `store_client_id` (`shop_user_id`);

--
-- Indexes for table `user_shops`
--
ALTER TABLE `user_shops`
  ADD PRIMARY KEY (`store_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_user`
--
ALTER TABLE `login_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `store_setting`
--
ALTER TABLE `store_setting`
  MODIFY `store_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_shops`
--
ALTER TABLE `user_shops`
  MODIFY `store_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
