//----------------------------- section for Invoice start here-------------------------------------

// console.log(location.hostname);
// console.log(document.domain);
// alert(window.location.hostname)

$(document).ready(function () {
  $(document).on("change", ".price", function () {
    var service_price = $(this).val();
    var service_qty = $(this).closest(".row").find(".quantity").val();
    var service_amt = service_qty * service_price;
    $(this).closest(".row").find(".amount").val(service_amt);
    sum = 0;
    $(".amount").each(function () {
      sum += +$(this).val();
    });
    $(".total-amt").text(sum);
    $(".final-amt").text(sum);
    var total_amt = $(".total-amt").text();
    var saletax = $(".saletax").val();
    var discount = $(".discount").val();
    console.log(saletax + "....saletax");
     if(saletax != ""){
      var per_saletax = (total_amt  * saletax) / 100;
      console.log(per_saletax + " .... per_saletax");
      var sum = parseFloat(total_amt) + parseFloat(per_saletax);
    }
    if(discount != ""){
      var per_discount = (sum * discount) / 100;
      var sum = parseFloat(sum) - (per_discount);
    }
    $(".final-amt").html(sum);
  });

  $(document).on("change", ".quantity", function () {
    var service_price = $(this).val();
    var service_qty = $(this).closest(".row").find(".price").val();
    var service_amt = service_qty * service_price;
    $(this).closest(".row").find(".amount").val(service_amt);
    sum = 0;
    $(".amount").each(function () {
      sum += +$(this).val();
    });
    
    $(".total-amt").text(sum);
    $(".final-amt").text(sum);
    var total_amt = $(".total-amt").text();
    var saletax = $(".saletax").val();
    var discount = $(".discount").val();
    console.log(saletax + "....saletax");
     if(saletax != ""){
      console.log(total_amt + "....total_amt");
      var per_saletax = (total_amt  * saletax) / 100;
      console.log(per_saletax + " .... per_saletax");
      var sum = parseFloat(total_amt) + parseFloat(per_saletax);
    }
    if(discount != ""){
      var per_discount = (sum * discount) / 100;
      var sum = parseFloat(sum) - (per_discount);
    }
    $(".final-amt").html(sum);

  });

  $(document).on("change", ".discount", function () {
    var discount = $(".discount").val();
    var total_amt = $(".total-amt").text();
    var tot = $(".final-amt").text();
    var saletax = $(".saletax").val();
    console.log(saletax + "....saletax");
     if(saletax != ""){
      var per_saletax = (total_amt  * saletax) / 100;
      console.log(per_saletax + " .... per_saletax");
      var total_amt = parseFloat(total_amt) + parseFloat(per_saletax);
    }
    var per_discount = (total_amt * discount) / 100;
    var final_discount = total_amt - per_discount;

    $(".final-amt").html(final_discount);
    console.log("discount");
    console.log(per_discount);
  });

  $(document).on("change", ".saletax", function () {
    var tax = $(".saletax").val();
    var total_amt = $(".total-amt").text();
    var tot = $(".final-amt").text();
    var discount = $(".discount").val();
    console.log(discount + "....discount");
    
    var pre_tax = (total_amt * tax) / 100;
    var saletax = parseInt(total_amt) + parseInt(pre_tax);
    console.log(saletax + "....saletax");
    if(discount != ""){
      var per_discount = (saletax * discount) / 100;
      console.log(per_discount + "....per_discount");
      var saletax = saletax - per_discount;
      console.log(saletax + "....saletax");
    }
    $(".final-amt").html(saletax);
    console.log("tax");
    console.log(pre_tax);
  });

  $("#submit").click(function (event) {
    console.log("submit button click");
    event.preventDefault();

    var form_data = $("#form")[0];
    var form_data = new FormData(form_data);
    $finalamt = $(".final-amt").html();
    form_data.append("finalamt",$finalamt);
    form_data.append("method", "insert_invoice");
    $(".clsServiceErrors").each(function (i, obj) {
      $(this).text("");
      $(this).closest(".row").find(".clsQtyErrors").text("");
      $(this).closest(".row").find(".clsPriceErrors").text("");
    });

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        $(".singleinvoiceErr").text("");
        $(".currencyErr").text("");
        $(".retainersErr").text("");
        $(".clientnameErr").text("");
        $(".invoicefromErr").text("");
        $(".clientdetailsErr").text("");
        $(".invoicedetailsErr").text("");
        $(".clientemailErr").text("");
        $(".invoicenoteErr").text("");
        $(".discountErr").text("");
        $(".saletaxErr").text("");
        $(".final-amt").text(0.00);
        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-1");
          if (returnResult["errors"]["singleinvoice"] != "") {
            $(".singleinvoiceErr").text(
              returnResult["errors"]["singleinvoice"]
            );
          }

          console.log("step-2");
          if (returnResult["errors"]["currency"] != "") {
            $(".currencyErr").text(returnResult["errors"]["currency"]);
          }

          console.log("step-3");
          if (returnResult["errors"]["retainers"] != "") {
            $(".retainersErr").text(returnResult["errors"]["retainers"]);
          }

          console.log("step-4");
          if (returnResult["errors"]["clientname"] != "") {
            $(".clientnameErr").text(returnResult["errors"]["clientname"]);
          }

          console.log("step-5");
          if (returnResult["errors"]["invoicefrom"] != "") {
            $(".invoicefromErr").text(returnResult["errors"]["invoicefrom"]);
          }
          console.log("step-6");
          if (returnResult["errors"]["clientdetails"] != "") {
            $(".clientdetailsErr").text(
              returnResult["errors"]["clientdetails"]
            );
          }

          console.log("step-7");
          if (returnResult["errors"]["invoicedetails"] != "") {
            $(".invoicedetailsErr").text(
              returnResult["errors"]["invoicedetails"]
            );
          }

          console.log("step-8");
          if (returnResult["errors"]["clientemail"] != "") {
            $(".clientemailErr").text(returnResult["errors"]["clientemail"]);
          }

          console.log("step-9");
          if (returnResult["errors"]["servicedescription"] != "") {
            $(".servicedescriptionErr").text(
              returnResult["errors"]["servicedescription"]
            );
          }

          console.log("step-10");
          if (returnResult["errors"]["invoicenote"] != "") {
            $(".invoicenoteErr").text(returnResult["errors"]["invoicenote"]);
          }

          console.log("step-11");
          if (returnResult["errors"]["discount"] != "") {
            $(".discountErr").text(returnResult["errors"]["discount"]);
          }

          console.log("step-12");
          if (returnResult["errors"]["saletax"] != "") {
            $(".saletaxErr").text(returnResult["errors"]["saletax"]);
          }

          $.each(
            returnResult["errors"]["servicedescription_error"],
            function (i, value) {
              $(".servicedescriptionErr" + i).text(value);
            }
          );

          $.each(
            returnResult["errors"]["serviceqty_error"],
            function (i, value) {
              $(".quantityErr" + i).text(value);
            }
          );

          $.each(
            returnResult["errors"]["serviceprice_error"],
            function (i, value) {
              $(".priceErr" + i).text(value);
            }
          );
        }
        if (returnResult["result"] == "success") {
          $("#form")[0].reset();
           setTimeout(function () {
          window.location = "invoicelisting.php";
        }, 2000);
        }
      },
    });
  });
});

//----------------------------- section for flows start here-------------------------------------

$(document).ready(function () {
  $("#create").click(function (event) {
    console.log("create button click");
    event.preventDefault();
    var form_data = $("#flowform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_flow");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".titleErr").text("");
        $(".imageErr").text("");
        $(".iconErr").text("");
        $(".briefErr").text("");
        $(".clienttagErr").text("");
        $(".teamErr").text("");
        $(".inviteclientErr").text("");
        $(".custommessageErr").text("");
        $(".flowdateErr").text("");
        
        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-0");
          if (returnResult["errors"]["flowdate"] != "") {
            $(".flowdateErr").text(returnResult["errors"]["flowdate"]);
          }
          if (returnResult["errors"]["team-members"] != "") {
            $(".membersErr").text(returnResult["errors"]["team-members"]);
          }
          console.log("step-1");
          if (returnResult["errors"]["title"] != "") {
            $(".titleErr").text(returnResult["errors"]["title"]);
          }
          console.log("step-3");
          if (returnResult["errors"]["file"] != "") {
            $(".imageErr").text(returnResult["errors"]["file"]);
          }
          console.log("step-4");
          if (returnResult["errors"]["brief"] != "") {
            $(".briefErr").text(returnResult["errors"]["brief"]);
          }
          console.log("step-5");
          if (returnResult["errors"]["clienttag"] != "") {
            $(".clienttagErr").text(returnResult["errors"]["clienttag"]);
          }
          console.log("step-6");
          if (returnResult["errors"]["team"] != "") {
            $(".teamErr").text(returnResult["errors"]["team"]);
          }
          console.log("step-7");
          if (returnResult["errors"]["inviteclient"] != "") {
            $(".inviteclientErr").text(returnResult["errors"]["inviteclient"]);
          }
           if (returnResult["errors"]["custommessage"] != "") {
            $(".custommessageErr").text(returnResult["errors"]["custommessage"]);
          }
        }
        if (returnResult["result"] == "success") {
          $(".success-1").text("submitted succesfully.");
          loadFlowData();
          setTimeout(function () {
            $(".filter-btn").show();
            $(".click-new-client").show();
            $("#flowform")[0].reset();
            $("#profile-pic").css("display", "block");
            $(".circle").css("background-color", "rgb(224, 255, 238)");
            $(".profile-pic").attr(
              "src",
              "https://t3.ftcdn.net/jpg/03/46/83/96/360_F_346839683_6nAPzbhpSkIpb8pmAwufkC7c5eD7wYws.jpg"
            );
            $(".success-1").text("");
            $(".flow-form").css("display", "none");
            $(".flow-data").css("display", "block");
          }, 2000);

          $(".click-new-client").click(function () {
            $(".flow-form").css("display", "block");
          });
        }
      },
    });
  });
});

//----------------------------- fetching data for flow start here-------------------------------------

function loadFlowData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow","status":"1"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if(returnResult['result'] == "fail"){
      console.log("hiiii fail");
      $(".error-flow").html(returnResult["msg"]);

    }else{
      console.log("hiiii success");
    //   $("#filterDropdown").css("display","block");
      $(".ui-sortable").html(returnResult["value"]);
      $(".active-flow-num").html(returnResult["active"]);
    $(".deactive-flow-num").html(returnResult["deactive"]);
    $(".deleted-flow-num").html(returnResult["deleted"]);
    }
    },
  });
}

function loadFlowDeletedData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow","status":"0"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
    $(".deleted-flow-num").html(returnResult["deleted"]);
    },
  });
}

function loadFlowDeactiveData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow","status":"2"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
    $(".deactive-flow-num").html(returnResult["deactive"]);
    },
  });
}
//----------------------------- fetching data for index flow start here-------------------------------------

function loadIndexFlowData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_indexflow" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".error-index-flow").text(returnResult["msg"]);
      $("#sortable").html(returnResult["value"]);
      $(".srno").html(returnResult["srno"]);
    },
  });
}

//----------------------------- section for client start here-------------------------------------

$(document).ready(function () {
  $("#cre").click(function (event) {
    console.log("create button click");
    event.preventDefault();
    var form_data = $("#clientform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_client");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".titleErr").text("");
        $(".imageErr").text("");
        $(".iconErr").text("");
        $(".briefErr").text("");
        $(".clienttagErr").text("");
        $(".teamErr").text("");
        $(".inviteclientErr").text("");
        $(".success").text("");
        $(".custommessageErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-1");
          if (returnResult["errors"]["title"] != "") {
            $(".titleErr").text(returnResult["errors"]["title"]);
          }
          console.log("step-3");
          if (returnResult["errors"]["file"] != "") {
            $(".imageErr").text(returnResult["errors"]["file"]);
          }
          console.log("step-4");
          if (returnResult["errors"]["brief"] != "") {
            $(".briefErr").text(returnResult["errors"]["brief"]);
          }
          console.log("step-5");
          if (returnResult["errors"]["clienttag"] != "") {
            $(".clienttagErr").text(returnResult["errors"]["clienttag"]);
          }
          console.log("step-6");
          if (returnResult["errors"]["team"] != "") {
            $(".teamErr").text(returnResult["errors"]["team"]);
          }
          console.log("step-7");
          if (returnResult["errors"]["inviteclient"] != "") {
            $(".inviteclientErr").text(returnResult["errors"]["inviteclient"]);
          }
          if (returnResult["errors"]["custommessage"] != "") {
            $(".custommessageErr").text(returnResult["errors"]["custommessage"]);
          }
        }
        if (returnResult["result"] == "success") {
          $(".success-1").text("submitted succesfully.");
          setTimeout(function () {
            loadClientData();
            $("#clientform")[0].reset();
             $(".filter-btn").show();
            $(".clientform").show();
            $("#profile-pic").css("display", "block");
            $(".circle").css("background-color", "rgb(224, 255, 238)");
            $(".profile-pic").attr(
              "src",
              "https://t3.ftcdn.net/jpg/03/46/83/96/360_F_346839683_6nAPzbhpSkIpb8pmAwufkC7c5eD7wYws.jpg"
            );
            $(".success").text("");
            $(".client-form").css("display", "none");
            $(".ul-display").css("display", "block");
          }, 2000);
        }
        $(".clientform").click(function () {
          $(".client-form").css("display", "block");
        });
      },
    });
  });
    
});
$(document).on("submit", "#inviteuserform", function(e) {
        e.preventDefault();
        console.log("hfuhfuhufhu");
        var form_data = $("#inviteuserform")[0];
        var form_data = new FormData(form_data);
        form_data.append("method", "inviteuserform");
        
        $.ajax({
         url: "response.php",
         type: "post",
         data: form_data,
         processData: false,
         contentType: false,

         success: function (returnResult) {
            var returnResult = JSON.parse(returnResult);
            console.log(returnResult);
            if (typeof returnResult["errors"] !== "undefined") {
                 $(".inviteemailErr").html("");
                 $(".custommessageErr").html("");
                  console.log(returnResult["errors"]["inviteemail"]);
            if (returnResult["errors"]["inviteemail"] !== "undefined" && returnResult["errors"]["inviteemail"] !== "" ) {
                console.log("if");
              $(".inviteemailErr").text(returnResult["errors"]["inviteemail"]);
            }else{
                console.log("else");
                $(".inviteemailErr").html("");
            }
            if (returnResult["errors"]["custommessage"] != "undefined" && returnResult["errors"]["custommessage"] != "") {
              $(".custommessageErr").text(returnResult["errors"]["custommessage"]);
            }else{
              $(".custommessageErr").html("");

            }
        }
        if (returnResult["result"] == "success") {
          setTimeout(function () {
            $("#inviteuserform")[0].reset();
            location.reload();
          }, 1000);
        }
       }
       });

    });
//----------------------------- section for client start here-------------------------------------

// $(document).ready(function() {

//   $("#cre").click(function(event) {
//     console.log("create button click");
//     event.preventDefault();
//     var form_data = $("#clientform")[0];
//     var form_data = new FormData(form_data);
//     form_data.append("method", "insert_users");

//     $.ajax({
//         url: "response.php",
//         type: "post",
//         data: form_data,
//         processData: false,
//         contentType: false,

//         success: function(returnResult) {
//             var returnResult = JSON.parse(returnResult);
//             // console.log(returnResult);
//             // return false;
//       },
//   });
// });
// });

//----------------------------- fetching data for client start here-------------------------------------

function loadClientData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_client","status":"1"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
       if(returnResult['result'] == "fail"){
        console.log("hiiii fail");
      $(".error-client").html(returnResult["msg"]);
  
      }else{
        // $("#filterDropdown").css("display","block");
      $(".ui-sortable").html(returnResult["value"]);
      $(".active-client-num").html(returnResult["active"]);
      $(".deactive-client-num").html(returnResult["deactive"]);
      $(".deleted-client-num").html(returnResult["deleted"]);
      }
    },
  });
}

function loadClientDeletedData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_client","status":"0"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".deleted-client-num").html(returnResult["deleted"]);
    },
  });
}

function loadClientDeactiveData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_client","status":"2"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".deactive-client-num").html(returnResult["deactive"]);
    },
  });
}

//----------------------------- section for task start here-------------------------------------

$(document).ready(function () {
  $("#tasksubmit").click(function (event) {
    console.log("submit button click");
    event.preventDefault();
    var form_data = $("#taskform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_task");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".tasknameErr").text("");
        $(".tasktypeErr").text("");
        $(".tasktitleErr").text("");
        $(".taskpriorityErr").text("");
        $(".taskdateErr").text("");
        $(".taskassignedErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-1");
          if (returnResult["errors"]["taskname"] != "") {
            $(".tasknameErr").text(returnResult["errors"]["taskname"]);
          }
          console.log("step-3");
          if (returnResult["errors"]["tasktype"] != "") {
            $(".tasktypeErr").text(returnResult["errors"]["tasktype"]);
          }
          console.log("step-4");
          if (returnResult["errors"]["tasktitle"] != "") {
            $(".tasktitleErr").text(returnResult["errors"]["tasktitle"]);
          }
          console.log("step-5");
          if (returnResult["errors"]["taskpriority"] != "") {
            $(".taskpriorityErr").text(returnResult["errors"]["taskpriority"]);
          }
          console.log("step-6");
          if (returnResult["errors"]["taskdate"] != "") {
            $(".taskdateErr").text(returnResult["errors"]["taskdate"]);
          }
          console.log("step-7");
          if (returnResult["errors"]["taskassigned"] != "") {
            $(".taskassignedErr").text(returnResult["errors"]["taskassigned"]);
          }
        }
        if (returnResult["result"] == "success") {
          $(".success-1").text("submitted succesfully.");

          setTimeout(function () {
            $("#taskform")[0].reset();
            $(".success").text("");
            $(".taskform").show();
            $(".task-form").css("display", "none");
            $(".task-table").css("display", "block");
            $(".table-body").css("display", "table-row-group");
            loadTaskData();
            return false;
          }, 2000);
        }
        $(".taskform").click(function () {
          $(".client-form").css("display", "block");
        });
      },
    });
  });
});

//----------------------------- fetching data for task start here-------------------------------------

function loadTaskData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_task","status": "0"},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      if (returnResult["result"] == "success") {
        $(".error-task").hide();
        $(".table-body").html(returnResult["value"]);
        $(".all-task-num").html(returnResult["total"]);
        $(".in-progress-task-num").html(returnResult["inprogress"]);
        $(".completed-task-num").html(returnResult["completed"]);
      }else{
        $(".error-task").show();
        $(".error-task").html(returnResult["msg"]);
      }
    },
  });
}

$(document).on("click",".active-task" ,function() {
  console.log("completed-tasks");
      
    $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_task","status":"0"},
      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
  
        if (returnResult["result"] == "success") {
          $(".error-task").hide();
          $(".table-body").show();
          $(".table-body").html(returnResult["value"]);
          // $(".all-tasks").html(returnResult["total"]);
          // $(".in-progress").html(returnResult["inprogress"]);
          $(".completed").html(returnResult["completed"]);
        }else{
          $(".table-body").hide();
          $(".error-task").show();
          $(".error-task").html(returnResult["msg"]);
        }
      },
    });
  
});

$(document).on("click",".deactive-task", function() {
  console.log("completed-tasks");
      
    $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_task","status":"1"},
      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
  
        if (returnResult["result"] == "success") {
          $(".error-task").hide();
          $(".table-body").show();
          $(".table-body").html(returnResult["value"]);
          // $(".all-tasks").html(returnResult["total"]);
          // $(".in-progress").html(returnResult["inprogress"]);
          $(".completed").html(returnResult["completed"]);
        }else{
          $(".table-body").hide();
          $(".error-task").show();
          $(".error-task").html(returnResult["msg"]);
        }
      },
    });
  
});

$(document).on("click",".deleted-task", function() {
  console.log("completed-tasks");
      
    $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_task","status":"2"},
      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
  
        if (returnResult["result"] == "success") {
          $(".error-task").hide();
          $(".table-body").show();
          $(".table-body").html(returnResult["value"]);
          // $(".all-tasks").html(returnResult["total"]);
          // $(".in-progress").html(returnResult["inprogress"]);
          $(".completed").html(returnResult["completed"]);
        }else{
          $(".table-body").hide();
          $(".error-task").show();
          $(".error-task").html(returnResult["msg"]);
        }
      },
    });
  
});

    $(document).on("click",".taskmark",function () {
      console.log("click");
      $(this).closest(".MuiTableRow-root").find(".check-mark").show();
      var clientid = $(this)
        .closest(".MuiTableRow-root")
        .find(".client-id")
        .text();
      var userid = $(this).closest(".MuiTableRow-root").find(".user-id").text();
      $(this).hide();

      console.log(clientid);
      console.log(userid);
      $.ajax({
        url: "response.php",
        type: "post",
        data: {
          method: "update_task",
          userid: userid,
          clientid: clientid,
          type: "circle",
        },
        success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);

          if (returnResult["result"] == "success") {
            $(".success").text("Submitted Succesfully.");
            setTimeout(function () {
              $(".success").text("");
              loadTaskData();
            }, 1000);
          }
        },
      });
    });

  $(document).on("click",".check-mark",function () {
      console.log("click");
      $(this).closest(".MuiTableRow-root").find(".taskmark").show();
      var clientid = $(this)
        .closest(".MuiTableRow-root")
        .find(".client-id")
        .text();
      var userid = $(this).closest(".MuiTableRow-root").find(".user-id").text();
      $(this).hide();

      $.ajax({
        url: "response.php",
        type: "post",
        data: {
          method: "update_task",
          userid: userid,
          clientid: clientid,
          type: "mark",
        },
        success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);

          $(".success").text("Submitted Succesfully.");
          setTimeout(function () {
            $(".success").text("");
            loadTaskData();
          }, 1000);
        },
      });
    });
  

//----------------------------- fetching task data for index task start here-------------------------------------

function loadIndexTaskData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_indextask" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".error-index-task").html(returnResult["msg"]);
      $(".table-body").html(returnResult["value"]);
      $(".tsrno").html(returnResult["srno"]);
    },
  });
}

//----------------------------- section for profile start here-------------------------------------

$(document).ready(function () {
  $("#user").click(function (event) {
    console.log("create button click");
    event.preventDefault();
    var form_data = $("#userform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_user");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);

        if (returnResult["result"] == "success") {
          $("#userform")[0].reset();
          $(".success").text("Submitted Succesfully.");
          setTimeout(function () {
            $(".success").text("");
            window.location = "index.php";
          }, 2000);
        }
      },
    });
  });
});

//----------------------------- section for invite user start here-------------------------------------

$(document).ready(function () {
  $("#sendinvitation").click(function (event) {
    console.log("invitation button click");
    event.preventDefault();
    var form_data = $("#inviteuserform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_inviteemail");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".inviteemailErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-1");
           $(".inviteemailErr").html(""); 
           $(".passwordErr1").html(""); 
           $(".cpasswordErr").text("");
           $(".custommessageErr").text("");
          if (returnResult["errors"]["inviteemail"] !== "undefined" && returnResult["errors"]["inviteemail"] !== "") {
              console.log("if");
            $(".inviteemailErr").text(returnResult["errors"]["inviteemail"]);
          }else{
            $(".inviteemailErr").html("");  
          }
          if (returnResult["errors"]["password1"] !== "undefined" && returnResult["errors"]["password1"] !== "") {
              console.log("if");
            $(".passwordErr1").text(returnResult["errors"]["password1"]);
          }else{
             $(".passwordErr1").html(""); 
          }
          if (returnResult["errors"]["cpassword"] !== "undefined" && returnResult["errors"]["cpassword"] !== "") {
              console.log("if");
            $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
          }else{
              $(".cpasswordErr").text("");
          }
          if (returnResult["errors"]["custommessage"] !== "undefined" && returnResult["errors"]["custommessage"] !== "") {
            $(".custommessageErr").text(returnResult["errors"]["custommessage"]);
          }else{
              $(".custommessageErr").text("");
          }
        }
        $(".click-user").on("click", function () {
          $(".invite-email").show();
        });
        if (returnResult["result"] == "success") {
          $(".success").text("Submitted Succesfully.");
          loadInviteEmailData();
          setTimeout(function () {
            $(".success").text("");
            $(".invite-email").hide();
            $(".main-team-section").show();
            $("#inviteuserform")[0].reset();
            loadProfileData();
          }, 1000);
        }
      },
    });
  });
});

//----------------------------- section for update brief start here-------------------------------------

function updateFlowBriefData(id) {
  $("#updateBrief").click(function (event) {
    console.log("invitation button click");
    event.preventDefault();
    var form_data = $("#flowbriefform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_flow_brief");
    form_data.append("id",id);

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        if (returnResult["result"] == "success") {
          $(".success").text("Updated Succesfully.");
          setTimeout(function () {
            $(".success").text("");
            $(".main-client-form").css("display", "none");
            $(".client-todo-list").css("display", "block");
            $(".brief-menu").css("display", "block");
            loadFlowClientData(id);
          }, 1000);
        }
      },
    });
  });
}


// function loadFlowClientData(id) {


//   $.ajax({
//     url: "response.php",
//     type: "post",
//     data: { method: "fetch_flow_details","id": id},
//     success: function (returnResult) {
//       var returnResult = JSON.parse(returnResult);
//       $(".profile-pic").attr("src","images/"+returnResult["icon"]+"");
//       $(".circle").css("background-color",""+returnResult["icon"]+"");
//       $(".float-client-name").html(returnResult["title"]);
//       $("#flowBriefTitle").val(returnResult["title"]);
//       $("#flowBriefDate").val(returnResult["flow_due_date"]);
//       $(".due-date-value").text(returnResult["flow_due_date"]);
//       $("#flowBriefTag").val(returnResult["flow_tag"]);
//       $(".textdetails").val(returnResult["brief"]);
//       $(".tag-value").html(returnResult["flow_tag"]);
//       $(".status-value").html(returnResult["status"]);
//     },
//   });
// }
//----------------------------- fetching data for team start here-------------------------------------

function loadInviteEmailData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_inviteemial" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".error-team").html(returnResult["msg"]);
      $(".inviteemail").html(returnResult["value"]);
      $(".srno-1").html("("+returnResult["srno"]+")");
    },
  });
}

//----------------------------- fetching data for client start here-------------------------------------

function loadInviteClientData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_inviteclient" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".error-team").html(returnResult["msg"]);
      $(".client-email").html(returnResult["value"]);
      $(".srno-2").html("("+returnResult["srno"]+")");
    },
  });
}

//----------------------------- section for create agency start here-------------------------------------

$(document).ready(function () {
  $("#createagency").click(function (event) {
    console.log("agency button click");
    event.preventDefault();
    var form_data = $("#createagencyform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_createagency");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".emailErr").text("");
        $(".workspaceErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          console.log("step-1");
          if (returnResult["errors"]["agencyemail"] != "") {
            $(".emailErr").text(returnResult["errors"]["agencyemail"]);
            console.log("step-2");
          }
          if (returnResult["errors"]["workspace"] != "") {
            $(".workspaceErr").text(returnResult["errors"]["workspace"]);
            console.log("step-3");
          }
        }
        if (returnResult["result"] == "success") {
          $("#createagencyform")[0].reset();
          $(".success").text("Submitted Succesfully.");
          setTimeout(function () {
            $(location).attr("href", "index.php");
          }, 1000);
        }
      },
    });
  });
});

//----------------------------- section for register start here-------------------------------------

$(document).ready(function () {
  $("#registration").click(function (event) {
    console.log("register button click");
    event.preventDefault();
    var form_data = $("#registrationform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_registration");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".firstnameErr").text("");
        $(".emailErr").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");
        $(".reg-loader").text("");
        $(".loader").css("display", "inline-block");
        setTimeout(function () {
          if (typeof returnResult["errors"] !== "undefined") {
            console.log("step-1");
            if (returnResult["errors"]["fistname"] != "") {
              $(".firstnameErr").text(returnResult["errors"]["fistname"]);
            }
            if (returnResult["errors"]["email"] != "") {
              $(".emailErr").text(returnResult["errors"]["email"]);
            }
            if (returnResult["errors"]["password"] != "") {
              $(".passwordErr").text(returnResult["errors"]["password"]);
            }
            if (returnResult["errors"]["cpassword"] != "") {
              $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
            }
          }
          if (returnResult["result"] == "mailerr") {
            $(".emailErr").text(returnResult["err"]);
          }
          $(".reg-loader").text(" Register Account");
          $(".loader").css("display", "none");
        }, 500);
        if (returnResult["result"] == "success") {
          $(".reg-loader").text("");
          $(".loader").css("display", "inline-block");
          $(".success").text("Register Succesfully.");
          setTimeout(function () {
            $("#registrationform")[0].reset();
            window.location = "login.php";
          }, 2000);
        }
      },
    });
  });
});

//----------------------------- section for login start here-------------------------------------

$(document).ready(function () {
  $(".loader").css("display", "none");

  $("#login").click(function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#loginform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_login");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".firstnameErr").text("");
        $(".emailErr").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");
        $(".login-loader").text("");
        $(".loader").css("display", "inline-block");
        setTimeout(function () {
          $(".login-loader").text("login");
          $(".loader").css("display", "none");

          if (returnResult["result"] == "success") {
            $(".login-loader").text("");
            $(".loader").css("display", "inline-block");
            setTimeout(function () {
              $("#loginform")[0].reset();
              window.location = "index.php";
            }, 1000);
          } else {
            $(".fail").text("Username or password is invalid");
          }
        }, 500);
      },
    });
  });
});

//----------------------------- section for profile photo change start here-------------------------------------
function prev() {
  $(document).ready(() => {
    $("#profileimg").change(function () {
      const file = this.files[0];
      console.log(file);
      if (file) {
        let reader = new FileReader();
        reader.onload = function (event) {
          console.log(event.target.result);
          $("#profileprev").attr("src", event.target.result);
        };
        reader.readAsDataURL(file);
      }
    });
  });
}
function prev1() {
  $(document).ready(() => {
    $("#agencyimg").change(function () {
      const file = this.files[0];
      console.log(file);
      if (file) {
        let reader = new FileReader();
        reader.onload = function (event) {
          console.log(event.target.result);
          $("#agencyprev").attr("src", event.target.result);
        };
        reader.readAsDataURL(file);
      }
    });
  });
}

//----------------------------- fetching data for agency start here-------------------------------------

function loadAgencyData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_agencyworkspace" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".profile-name").html(returnResult["username"]);
      $(".user-name").html(returnResult["username"]);
      $(".profile-class").html(returnResult["value"]);
      $(".error-agency").html(returnResult["msg"]);
    },
  });
}

//----------------------------- fetching data for agency start here-------------------------------------

function profileName() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_profile_name" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".profile-name").html(returnResult["username"]);
    },
  });
}

//----------------------------- fetching data for chats start here-------------------------------------

function loadChatsData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_customers" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".chats-add").html(returnResult["value"]);
      $(".error-chat-friends").html(returnResult["msg"]);
      $(".user-friends").html(returnResult["value"]);
      if (returnResult["result"] == "fail") {
        $(".chats-add").html(returnResult["msg"]);
        $(".chat-header-user").css("display", "none");
        // $(".chat-footer").css("display", "none");
        $(".chat-header-action").css("margin-left", "96%");
      }
      if (returnResult["result"] == "success") {
        $(".layout").css("display", "flex");
        $(".error-chat-friends").css("display", "none");
        // var userid = $(".user-id").html();

        dddd();
      }
      firstClientName();
    },
  });
}

function dddd() {
  var userid = $(".user-id").html();
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_admin", userid: userid },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".chats-admin").html(returnResult["value"]);
      // setTimeout(loadChatsData, 5000);
      firstClientName();
    },
  });
}

function firstClientName() {
  $clientid = $(".CLSAGGGG li:first ").find(".client-id").html();
  $username = $(".CLSAGGGG li:first .username").html();
  $(".chat-name").html($username);
  $(".recevierId").val($clientid);
  $(".recevierName").val($username);
}


//----------------------------- fetching data for chats start here-------------------------------------

function loadFlowList() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow_list" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".flow-list").html(returnResult["value"]);
      $(".error-chat-friends").html(returnResult["msg"]);
      $(".user-friends").html(returnResult["value"]);
      if (returnResult["result"] == "fail") {
        $(".chat-header-user").css("display", "none");
        // $(".chat-footer").css("display", "none");
        $(".flow-list").html(returnResult["msg"]);
        $(".chat-header-action").css("margin-left", "96%");
      }
      if (returnResult["result"] == "success") {
        $(".layout").css("display", "flex");
        $(".error-chat-friends").css("display", "none");
        // var userid = $(".user-id").html();
      }
      
    },
  });
}

//----------------------------- fetching data for chats start here-------------------------------------

function loadClientList() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_client_list" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".client-list").html(returnResult["value"]);
      $(".error-chat-friends").html(returnResult["msg"]);
      if (returnResult["result"] == "fail") {
        $(".client-list").html(returnResult["msg"]);
        $(".chat-header-user").css("display", "none");
        $(".client-list").html(returnResult["msg"]);
        $(".chat-header-action").css("margin-left", "96%");
      }
      if (returnResult["result"] == "success") {
        $(".layout").css("display", "flex");
        $(".error-chat-friends").css("display", "none");
      }
      
    },
  });
}

//----------------------------- fetching data for chats start here-------------------------------------

function loadGroupData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_group_data" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".members").html(returnResult["value"]);
      $(".members-id").html(returnResult["value"]);
      $(".team-members").html(returnResult["value"]);
      $(".assigned-task").html(returnResult["value"]);
      $(".members-list-append").html(returnResult["value"]);
      $(".length").html(returnResult["length"]);
      $("#clientName").html(returnResult["name"]);
    },
  });
}

//----------------------------- fetching data for user profile start here-------------------------------------

function loadProfileData($datarole) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_profiledata", role: $datarole },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $("#f-name").val(returnResult["name"]);
      $("#l-name").val(returnResult["lastname"]);
      $("#email").val(returnResult["email"]);
      $("#p-name").val(returnResult["username"]);
      $("#a-name").val(returnResult["agency"]);
      console.log(returnResult["userprofile"]);
      if (
        returnResult["userprofile"] == "" ||
        returnResult["userprofile"] == null
      ) {
        $("#profileprev").attr(
          "src",
          "https://sweetp-user-uploads.s3.eu-west-2.amazonaws.com/stage%20/%201654253008180_profile"
        );
      } else {
        $("#profileprev").attr("src", "images/" + returnResult["userprofile"]);
         $(".img-profile").attr("src", "images/" + returnResult["userprofile"]);
      }
      if (
        returnResult["agencyprofile"] == "" ||
        returnResult["agencyprofile"] == null
      ) {
        $("#agencyprev").attr(
          "src",
          "https://sweetp-user-uploads.s3.eu-west-2.amazonaws.com/stage%20/%201654253008180_profile"
        );
      } else {
        $("#agencyprev").attr("src", "images/" + returnResult["agencyprofile"]);
      }
    },
  });
}

//----------------------------- fetching last msg for chat start here-------------------------------------

function loadLastMsg() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_lastmsg" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".user-name").html(returnResult["username"]);
    },
  });
}

//----------------------------- fetching data for index recent chat start here-------------------------------------

function loadIndexChat() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_indexchat" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".error-index-chat").html(returnResult["msg"]);
      $(".recent-chat").html(returnResult["value"]);
      $(".chat-srno").html(returnResult["srno"]);
    },
  });
}

//----------------------------- section for chat-msg start here-------------------------------------

$(document).ready(function () {
  $("#sendMsg").click(function () {
    console.log("send button click");
    $(".close-btncls").css("display","none");
    $(".cls-lots").removeClass("show");
    $(".prev-img").hide();
    var form_data = $("#formMsg")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_chatmsg");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
      },
    });
  });
});

//----------------------------- section for create password for user start here-------------------------------------
$(document).ready(function () {
  $("#createpass").click(function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#passwordform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_password");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          if (returnResult["errors"]["password"] != "") {
            $(".passwordErr").text(returnResult["errors"]["password"]);
          }
          if (returnResult["errors"]["cpassword"] != "") {
            $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
          }
        }
        if (returnResult["result"] == "success") {
          setTimeout(function () {
            $("#loginform")[0].reset();
            window.location = "index.php";
          }, 1000);
        }
      },
    });
  });
  });

//----------------------------- section for create password for user start here-------------------------------------
$(document).ready(function () {
  $("#createteampass").on('click',function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#teampasswordform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_team_password");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          if (returnResult["errors"]["password"] != "") {
            $(".passwordErr").text(returnResult["errors"]["password"]);
          }
          if (returnResult["errors"]["cpassword"] != "") {
            $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
          }
        }
        if (returnResult["result"] == "success") {
          $(".success").text("Updated Successfully.")
          setTimeout(function () {
            $("#teampasswordform")[0].reset();
            window.location = "index.php";
          }, 1000);
        }
      },
    });
  });
  });

//----------------------------- fetching data for index recent chat start here-------------------------------------
function loadTeamData(id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow_team","id":id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
    //   console.log(returnResult["value"]);
    $(".team-table").append(returnResult["value"]);
    $(".members").append(returnResult["assignuser"]);
    $(".other").append(returnResult["option"]);
    },
  });
}

//----------------------------- fetching data for index recent chat start here-------------------------------------
function deleteTeamData(id) {
  setTimeout(function()  {
    $(".team-btn").on('click',function(){
console.log("delete btn click");
var name = $(this).closest("tr").find(".member-name").text();
console.log(name);
$(this).closest("tr").remove();

  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "delete_flow_team","id":id,"name[]":name},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".delete-msg").text("Deleted Successfully.");
        setTimeout(function () {
          $(".delete-msg").text("");
          location.reload(true);
        }, 2000);
      }
    },
  });
});
}, 1000);
}

//----------------------------- section for create password for user start here-------------------------------------

  function updateFlowTeam(id){
  $("#addTeamUser").click(function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#inviteTeamUser")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_flow_team");
    form_data.append("id", id);

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        if (returnResult["result"] == "success") {
          $(".success").text("Added Successfully.");
          setTimeout(function () {
            $(".success").text("");
            $("#inviteTeamUser")[0].reset();
            location.reload(true);
          }, 1000);
        }
      },
    });
  });
}

//----------------------------- fetching data for index recent chat start here-------------------------------------

function loadCreateEmail($id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_createemail", id: $id },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      $(".fetch-email").html(returnResult["value"]);
    },
  });
}
function loadCreateinviteEmail($id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_email_inviteuser", id: $id },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      $(".fetch-email").html(returnResult["value"]);
    },
  });
}
function loadTeamEmail($id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_team_email", id: $id },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      $(".fetch-email").html(returnResult["value"]);
    },
  });
}

function removeFromTable(tableName, ID, is_delete) {
  var is_delete = is_delete == undefined ? "Record" : is_delete;
  var Ajaxdelete = function Ajaxdelete() {
    var el = is_delete;
    $.ajax({
      url: "response.php",
      type: "post",
      dataType: "json",
      data: { method: "remove_from_table", table_name: tableName, ID: ID },

      success: function (response) {
        if (response["result"] == "success") {
          $(is_delete).closest("tr").css("background", "tomato");
          $(is_delete)
            .closest("tr")
            .fadeOut(800, function () {
              $(this).remove();
            });
        } else {
          window.location = "index.php?store=" + store;
          setCookie("flash_message", response["message"], 2);
        }
      },
    });
  };

  var r = confirm("Are you sure want to delete!");
  if (r == true) {
    Ajaxdelete();
  }
}

//----------------------------- fetching data for create password page start here-------------------------------------

function loadCustomerPass($id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_password", id: $id },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      if (returnResult["result"] == "fail") {
        $(".pass").hide();
        $(".cpass").hide();
        $("#createpass").hide();
        $(".mail-msg").text(returnResult["msg"]);
      } else {
      }
    },
  });
}

//----------------------------- fetching data for create password page start here-------------------------------------

function loadTeamPass($id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_team_password", id: $id },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      if (returnResult["result"] == "fail") {
          $(".pass").hide();
          $(".cpass").hide();
          $("#createteampass").hide();
       
        $(".mail-msg").text(returnResult["msg"]);
      } else {
      }
    },
  });
}

//----------------------------- section for new group for chat start here-------------------------------------

$(document).ready(function () {
  $("#createGroup").click(function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#newgroup")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "insert_group");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          if (returnResult["errors"]["groupname"] != "") {
            $(".groupnameErr").text(returnResult["errors"]["groupname"]);
          }
          if (returnResult["errors"]["membername"] != "") {
            $(".membersErr").text(returnResult["errors"]["membername"]);
          }
          if (returnResult["errors"]["description"] != "") {
            $(".descriptionErr").text(returnResult["errors"]["description"]);
          }
        }
        if (returnResult["result"] == "success") {
          setTimeout(function () {
            $("#newgroup")[0].reset();
            window.location = "chat.php";
          }, 1000);
        }
      },
    });
  });
});

//----------------------------- section for chnage passsword start here start here-------------------------------------

$(document).ready(function () {
  $("#changePass").click(function (event) {
    console.log("login button click");
    event.preventDefault();
    var form_data = $("#updatepassword")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_pass");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".currentpasswordErr").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          if (returnResult["errors"]["currentpassword"] != "") {
            $(".currentpasswordErr").text(
              returnResult["errors"]["currentpassword"]
            );
          }
          if (returnResult["errors"]["password"] != "") {
            $(".passwordErr").text(returnResult["errors"]["password"]);
          }
          if (returnResult["errors"]["cpassword"] != "") {
            $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
          }
        }
        if (returnResult["result"] == "success") {
          setTimeout(function () {
            $("#updatepassword")[0].reset();
            window.location = "logout.php";
          }, 1000);
        }
      },
    });
  });
});

function getTags() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "getTags" },
    success: function (returnResult) {
      console.log(returnResult);
      return false;
      var returnResult = JSON.parse(returnResult);
      console.log(returnResult);
      // $(".user-name").html(returnResult["username"]);
      // $(".profile-class").html(returnResult["value"]);
      // $(".error-agency").html(returnResult["msg"]);
    },
  });
}

//----------------------------- fetching data for chats start here-------------------------------------

function loadGroup() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_group" },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".group-members").html(returnResult["value"]);
      $(".error-chat-friends").html(returnResult["msg"]);
      $(".user-friends").html(returnResult["value"]);
      if (returnResult["result"] == "fail") {
        $(".chat-header-user").css("display", "none");
        // $(".chat-footer").css("display", "none");
        $(".chat-header-action").css("margin-left", "96%");
      }
      if (returnResult["result"] == "success") {
        $(".layout").css("display", "flex");
        $(".error-chat-friends").css("display", "none");
        // var userid = $(".user-id").html();
      }
    },
  });
}

//----------------------------- delete flow notes start here-------------------------------------
$(document).on("click",".delete-note-flow",function(){
console.log("hiiii");
  $(this).closest("tr").hide(); 



  var id = $(this).closest("tr").find(".id").text(); 
  var userid = $(this).closest("tr").find(".user-id").text(); 
  var flowid = $(this).closest("tr").find(".task-status").text(); 
  
  console.log(id);
  console.log(userid);

  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "delete_flow_notes","id":id,"userid":userid },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".success").text("deleted Successfully.");
        setTimeout(function()  {
          $(".success").text("");
          loadFlowNotes(flowid);
        }, 1000);
      }
    },
  });
  });  

$(document).on("click",".delete-note-client",function(){
console.log("hiiii");
  $(this).closest("tr").hide(); 



  var id = $(this).closest("tr").find(".id").text(); 
  var userid = $(this).closest("tr").find(".user-id").text(); 
  var clientid = $(this).closest("tr").find(".task-status").text(); 

  
  console.log(id);
  console.log(userid);

  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "delete_flow_notes","id":id,"userid":userid },
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".success").text("deleted Successfully.");
        setTimeout(function()  {
          $(".success").text("");
          loadClientNotes(clientid);
        }, 1000);
      }
    },
  });
  });  


//----------------------------- fetch flow notes start here-------------------------------------
function loadClientNotes(id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_client_notes","client-id":id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      if (returnResult["result"] == "success") {
        $(".error-task").hide();
        $(".table-bodys").html(returnResult["value"]);
      }else{
        $(".error-task").show();
        $(".error-task").html(returnResult["msg"]);
      }
    },
  });
}

function loadFlowNotes(id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow_notes","client-id":id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);

      if (returnResult["result"] == "success") {
        $(".error-task").hide();
        $(".table-bodys").html(returnResult["value"]);
      }else{
        $(".error-task").show();
        $(".error-task").html(returnResult["msg"]);
      }
    },
  });
}
// flow
$(".Click-here").on('click', function() {
  $(".custom-model-main").addClass('model-open');
});
$(".close-btn, .bg-overlay").click(function() {
  $(".custom-model-main").removeClass('model-open');
});

function loadFlowClientData(id) {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_flow_details","id": id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      $(".profile-pic").attr("src","images/"+returnResult["icon"]+"");
      $(".circle").css("background-color",""+returnResult["icon"]+"");
      $(".float-client-name").html(returnResult["title"]);
      $("#flowBriefTitle").val(returnResult["title"]);
      $("#flowBriefDate").val(returnResult["flow_due_date"]);
      $(".due-date-value").text(returnResult["flow_due_date"]);
      $("#flowBriefTag").val(returnResult["flow_tag"]);
      $(".textdetails").val(returnResult["brief"]);
      $(".tag-value").html(returnResult["flow_tag"]);
      $(".status-value").html(returnResult["flow_status"]);
      if(returnResult["flow_status"] == "Active"){
        $(".reactive-flow-brief").hide();
      }
      else if(returnResult["flow_status"] == "Deleted"){
        $(".reactive-flow-brief").show();
        $(".delete-flow-brief").hide();
        $(".complete-flow-brief").hide();
      }else{
        $(".reactive-flow-brief").show();
        $(".delete-flow-brief").hide();
        $(".complete-flow-brief").hide();
      }
    },
  });
}


function loadClientTeamData(id){
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_clientteam_details","id": id},
    success: function (returnResult) {
      
      var returnResult = JSON.parse(returnResult);
      console.log(returnResult);
      $(".team-table").append(returnResult["value"]);
      $(".members").append(returnResult["assignuserhtml"]);
      $(".other").append(returnResult["option"]);
      $(".float-client-name").html(returnResult["title"]);
    },
  });
}
function deleteclientTeamData(id) {
  setTimeout(function()  {
    $(".team-btn").on('click',function(){
    console.log("delete btn click");
    var name = $(this).closest("tr").find(".member-name").text();
    console.log(name);
    $(this).closest("tr").remove();

  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "delete_client_team","id":id,"name[]":name},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".delete-msg").text("Deleted Successfully.");
        setTimeout(function () {
          $(".delete-msg").text("");
        }, 2000);
      }
    },
  });
});
}, 1000);
}
function addClientTeamUser(id){
  var form_data = $("#inviteTeamUser")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_client_team");
    form_data.append("id", id);

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        if (returnResult["result"] == "success") {
          $(".success").text("Added Successfully.");
          setTimeout(function () {
            $(".success").text("");
            $("#inviteTeamUser")[0].reset();
            location.reload(true);
          }, 1000);
        }
      },
    });
}


$(document).ready(function () {
  $("#updateBriefclient").on("click",function (event) {
    console.log("login button click");
    event.preventDefault();
    var id = $(".user-id").html();
    var form_data = $("#clientbriefform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_client_brief");
    form_data.append("id", id);

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        
        if (returnResult["result"] == "success") {
          $(".success").html("Updated Successfully.");
          setTimeout(function () {
            $(".success").html("");
            $(".main-client-form").css("display", "none");
            $(".client-todo-list").css("display", "block");
            $(".brief-menu").css("display", "block");
            $("#clientbriefform")[0].reset();
          }, 1000);
        }
      },
    });
  });
  });

// ----------------------------client filter---------------------------------------------
  $(document).ready(function () {
$(".active-client").click(function(){
    
  $(".deactivate-clients").hide();
  $(".deleted-clients").hide();
  $(".ui-sortable").show();
  $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_client","status":"1"},
      success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);
          $(".error-client").html(returnResult["msg"]);
    $(".ui-sortable").html(returnResult["value"]);
  },
});
});
$(".deactive-client").click(function(){
  setTimeout(function() {
      // $(".change-option").css("display","block");
  $(".change-option").show();
}, 100);
  $(".deactivate-clients").show();
  $(".deleted-clients").hide();
  $(".ui-sortable").hide();
  $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_client","status":"2"},
      success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);
          $(".error-client").html(returnResult["msg"]);
    $(".deactivate-clients").html(returnResult["value"]);
  },
});
});

$(".deleted-client").click(function(){
  setTimeout(function() {
      // $(".change-option").css("display","block");
  $(".change-option").show();
}, 100);
  $(".deleted-clients").show();
  $(".ui-sortable").hide();
  $(".deactivate-clients").hide();
  $.ajax({
  url: "response.php",
  type: "post",
  data: { method: "fetch_client","status":"0"},
  success: function (returnResult) {
    var returnResult = JSON.parse(returnResult);
    $(".error-client").html(returnResult["msg"]);
    $(".deleted-clients").html(returnResult["value"]);
  },
});
});
});



// -----------------------------------------------Flow filter --------------------------
  $(document).ready(function () {
$(".active-flow").click(function(){
    
  $(".deactivate-flow").hide();
  $(".delet-flow").hide();
  $(".ui-sortable").show();
  $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_flow","status":"1"},
      success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);
          $(".error-flow").html(returnResult["msg"]);
    $(".ui-sortable").html(returnResult["value"]);
    if (returnResult["result"] == "success") {
    $(".active-flow-num").html(returnResult["active"]);
    }
  },
});
});
$(".deactive-flow").click(function(){
  setTimeout(function() {
      // $(".change-option").css("display","block");
  $(".change-option").show();
}, 100);
  $(".deactivate-flow").show();
  $(".delet-flow").hide();
  $(".ui-sortable").hide();
  $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_flow","status":"2"},
      success: function (returnResult) {
          var returnResult = JSON.parse(returnResult);
          $(".error-flow").html(returnResult["msg"]);
    $(".deactivate-flow").html(returnResult["value"]);
    $(".deactive-flow-num").html(returnResult["deactive"]);
  },
});
});

$(".deleted-flow").click(function(){
  setTimeout(function() {
      // $(".change-option").css("display","block");
  $(".change-option").show();
}, 100);
  $(".delet-flow").show();
  $(".ui-sortable").hide();
  $(".deactivate-flow").hide();
  $.ajax({
  url: "response.php",
  type: "post",
  data: { method: "fetch_flow","status":"0"},
  success: function (returnResult) {
    var returnResult = JSON.parse(returnResult);
    $(".error-flow").html(returnResult["msg"]);
    $(".delet-flow").html(returnResult["value"]);
    $(".deleted-flow-num").html(returnResult["deleted"]);
  },
});
});
});


$(document).ready(function () {
  $(".delete-flow-brief").on("click",function(){
    var flow_id = $(this).find(".flow-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_flow_brief_status","status":"0","flow_id":flow_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".flow-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "flow.php";
      }, 1000);
      }
    },
  });
  });

  $(".complete-flow-brief").on("click",function(){
    var flow_id = $(this).find(".flow-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_flow_brief_status","status":"2","flow_id":flow_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".flow-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "flow.php";
      }, 1000);
      }
    },
  });
  });

  $(".reactive-flow-brief").on("click",function(){
    var flow_id = $(this).find(".flow-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_flow_brief_status","status":"1","flow_id":flow_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".flow-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "flow.php";
      }, 1000);
      }
    },
  });
  });
  
});


$(document).ready(function () {
  $(".delete-client-brief").on("click",function(){
    var client_id = $(this).find(".client-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_client_brief_status","status":"0","client_id":client_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".client-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "client.php";
      }, 1000);
      }
    },
  });
  });

  $(".complete-client-brief").on("click",function(){
    var client_id = $(this).find(".client-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_client_brief_status","status":"2","client_id":client_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".client-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "client.php";
      }, 1000);
      }
    },
  });
  });

  $(".reactive-client-brief").on("click",function(){
    var client_id = $(this).find(".client-brief-id").html();
    $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "update_client_brief_status","status":"1","client_id":client_id},
    success: function (returnResult) {
      var returnResult = JSON.parse(returnResult);
      if (returnResult["result"] == "success") {
        $(".client-success").html(returnResult["msg"]);
        setTimeout(function() {
        window.location = "client.php";
      }, 1000);
      }
    },
  });
  });
  
});


  function loadClientClientData(id) {

    

    $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_client_details","id": id},
      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
  
        if(returnResult["type"] == "0"){
          $("#indoor").prop("checked", true)
        }
        else{
          $("#outdoor").prop("checked", true)
        }
  
        $(".profile-pic").attr("src","images/"+returnResult["icon"]+"");
        $(".circle").css("background-color",""+returnResult["icon"]+"");
        $(".float-client-name").html(returnResult["title"]);
        $("#clientBriefTitle").val(returnResult["title"]);
        $("#flowBriefDate").val(returnResult["flow_due_date"]);
        $(".due-date-value").text(returnResult["flow_due_date"]);
        $("#clientBriefTag").val(returnResult["tag"]);
        $(".float-client-name").html(returnResult["title"]);
        $(".textdetails").val(returnResult["brief"]);
        $(".tag-value").html(returnResult["tag"]);
        $(".status-value").html(returnResult["client_status"]);
        if(returnResult["client_status"] == "Deleted"){
          $(".delete-client-brief").hide();
          $(".complete-client-brief").hide();
          $(".reactive-client-brief").show();
        }else if(returnResult["client_status"] == "Deactive"){
          $(".delete-client-brief").hide();
          $(".complete-client-brief").hide();
          $(".reactive-client-brief").show();
        }else{
          $(".delete-client-brief").show();
          $(".complete-client-brief").show();
          $(".reactive-client-brief").hide();
        }
      },
    });
  }


  $(document).ready(function () {

    $("#updateclientbrief").click(function (event) {
      console.log("create button click");
      event.preventDefault();
      var myContent = tinymce.activeEditor.getContent(); 
      $(".textdetails").val(myContent);
    var form_data = $("#clientbrief")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_clientbrief");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);

        if (returnResult["result"] == "success") {
          $(".success").text("Updated Succesfully.");
          setTimeout(function () {
            $(".success").text("");
            loadClientClientData();
          }, 2000);
        }
      },
    });
  });

  });
  function loadCustomerinvitePass($id) {
    $.ajax({
      url: "response.php",
      type: "post",
      data: { method: "fetch_password_inviteuser", id: $id },
      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
  
        if (returnResult["result"] == "fail") {
          $(".pass").hide();
          $(".cpass").hide();
          $("#createpass").hide();
          $(".mail-msg").text(returnResult["msg"]);
        } else {
        }
      },
    });
  }
  $(document).on("click","#createpassinviteuser",function (event) {
    console.log("login button click");
    event.preventDefault(); 
    var form_data = $("#passwordform")[0];
    var form_data = new FormData(form_data);
    form_data.append("method", "update_password_inviteuser");

    $.ajax({
      url: "response.php",
      type: "post",
      data: form_data,
      processData: false,
      contentType: false,

      success: function (returnResult) {
        var returnResult = JSON.parse(returnResult);
        // console.log(returnResult);
        // return false;
        $(".success").text("");
        $(".passwordErr").text("");
        $(".cpasswordErr").text("");

        if (typeof returnResult["errors"] !== "undefined") {
          if (returnResult["errors"]["password"] != "") {
            $(".passwordErr").text(returnResult["errors"]["password"]);
          }
          if (returnResult["errors"]["cpassword"] != "") {
            $(".cpasswordErr").text(returnResult["errors"]["cpassword"]);
          }
        }
        if (returnResult["result"] == "success") {
          $(".success").text("Password Updated Successfully.");
          setTimeout(function () {
            $(".success").text("");
            $("#passwordform")[0].reset();
            window.location = "login.php";
          }, 2000);
        }
      },
    });
  });
  
  
function loadInvoiceData() {
  $.ajax({
    url: "response.php",
    type: "post",
    data: { method: "fetch_invoice"},
    success: function (returnResult) {
      
      var returnResult = JSON.parse(returnResult);
      console.log(returnResult);
      if (returnResult["result"] == "success") {
      $(".table-bodys").append(returnResult["value"]);
      }else{
        $(".error-task").append(returnResult["msg"]);
      }
    },
  });
} 