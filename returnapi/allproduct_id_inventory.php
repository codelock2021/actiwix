<?php
  
   include_once '../append/connection.php';
   include_once ABS_PATH . '/user/cls_header.php';

class product_functions extends common_function {
  

    public function __construct($store = '') {
        $url = $_SERVER['REQUEST_URI'];
        $url_components = parse_url($url);
        
        $path = explode("/",$url_components['path']);
        // $shopify_api = $path[3];
        $query = isset($url_components['query']) ? $url_components['query'] : '';
        parse_str($query, $params);
       $store = (isset($params['store']) && $params['store'] != '' )  ? $params['store'] : "dashboardmanage.myshopify.com";
         
		if($store == "clsapps.myshopify.com"){
            $password="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";
        }else if($store == "dashboardmanage.myshopify.com"){
            $password="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"; 
        }else if($store == "cls-rewriter.myshopify.com"){
            $password="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"; 
        }else if($store == "managedashboard.myshopify.com"){
            $password="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"; 
        }
		$api_key="7ced2ccec20a621d975386679497fffa";
		$end_point = "https://$store/admin/api/graphql.json";
		$type = "POST";
		$query = "";
		$shopify_data_list = $this->cls_api_graphql_call($api_key, $password, $store, $end_point, $query, $type);
	
    }
  

function cls_api_graphql_call($api_key, $password, $store, $shopify_endpoint, $query = '', $type = '', $request_headers = array()) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $shopify_endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => "{
  products(first: 10) {
    edges {
      node {
        id
        variants(first: 10) {
          edges {
            node {
              id
              inventoryQuantity
            }
          }
        }
      }
    }
  }
}",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/graphql",
            "X-Shopify-Access-Token:$password"
        ),
    ));

    $comeback = curl_exec($curl);
   print_r($comeback);
    // print_r(json_encode($comeback));
    $err = curl_error($curl);
    curl_close($curl);
    return array('response' => $comeback);
}
}
$CF_obj = new product_functions($store);
?>