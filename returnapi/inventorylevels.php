<?php
   
   include_once '../append/connection.php';
   include_once ABS_PATH . '/user/cls_header.php';

class product_functions extends common_function {
  

    public function __construct($store = '') {
        $url = $_SERVER['REQUEST_URI'];
        $url_components = parse_url($url);
    
        parse_str($url_components['query'], $params);
         $store = (isset($params['store']) && $params['store'] != '' ) ? $params['store'] : "dashboardmanage.myshopify.com";
         
         $sku =( isset($params['sku']) && $params['sku'] != '') ? $params['sku'] : $returnArray["sku"] = 'Sku not found';
         $available = (isset($params['available']) && $params['available'] != '') ? $params['available'] : $returnArray["available"] = 'Available QTY not found';
        
        if($store == "clsapps.myshopify.com"){
            $password="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";
        }else if($store == "dashboardmanage.myshopify.com"){
            $password="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"; 
        }else if($store == "cls-rewriter.myshopify.com"){
            $password="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"; 
        }else if($store == "managedashboard.myshopify.com"){
            $password="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"; 
        }
		$api_key="7ced2ccec20a621d975386679497fffa";
		$end_point = "https://$store/admin/api/graphql.json";
		if(empty($returnArray)){
    		$shopify_data_list = $this->cls_api_graphql_call($api_key, $password, $store, $end_point, $sku, 'POST');
            $decode_data = json_decode($shopify_data_list["response"]);
            $checksku = $decode_data->data->productVariants->edges;
            if (!empty($checksku)) {
                    $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                    $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);
                    $end_point = "/admin/api/2022-10/locations.json";
                    // $fields = "";
            	    $get_level = $this->clsreturn_api_call($api_key, $password, $store, $end_point,  $query = array(), 'GET');	
            	    $decodedata = json_decode($get_level['response']);
            	    $locaionid = isset($decodedata->locations[0]->id)?$decodedata->locations[0]->id:'';
        	   	
                    $end_point2 ="/admin/api/2022-10/inventory_levels/set.json";
                    $filed_arr = array('location_id' => $locaionid, 'inventory_item_id' => $inventoryItem_id, 'available' =>$available);
                    $update_inventory = $this->clsreturn_api_call($api_key, $password, $store, $end_point2, $filed_arr, 'POST');
                }
		}else{
		     print_r($returnArray);
		    die;
		}
    }

function cls_api_graphql_call($api_key, $password, $store, $shopify_endpoint, $query = '', $type = '', $request_headers = array()) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $shopify_endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => "{ 
            productVariants(first: 10, query: \"sku:$query\") {
              edges {
              node {
                id
                title
                sku
                inventoryItem {
                id
                }
                displayName
                inventoryQuantity
                product {
                  id
                  title
                }
              }
            }
            }
          }",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/graphql",
            "X-Shopify-Access-Token:$password"
        ),
    ));

    $comeback = curl_exec($curl);
    // echo "<pre>";
    // print_r(json_decode($comeback));
    $err = curl_error($curl);
    curl_close($curl);
    return array('response' => $comeback);
}
 function clsreturn_api_call($api_key , $password, $store, $shopify_endpoint, $query = array(),$type = '', $request_headers = array()) {
    $cls_shopify_url = "https://" . $api_key .":". $password ."@". $store.  $shopify_endpoint;

     if (!is_array($type) && !is_object($type)) {
        (array)$type;
    }
	if (!is_null($query) && in_array($type,array('GET','DELETE'))) $cls_shopify_url = $cls_shopify_url . "?" . http_build_query($query);
	$curl = curl_init($cls_shopify_url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	$request_headers[] = "";
    $request_headers[] ="Content-Security-Policy, https:".$store." https://admin.shopify.com";
	
	if (!is_null($password)) $request_headers[] = "X-Shopify-Access-Token: " . $password;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
	if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
		if (is_array($query)) $query = http_build_query($query);
    		curl_setopt($curl, CURLOPT_POSTFIELDS,$query);
	}   

	$comeback = curl_exec($curl);
	$error_number = curl_errno($curl);
	$error_message = curl_error($curl);
	curl_close($curl);
	if ($error_number) {
		return $error_message;
	} else {
		$comeback = preg_split("/\r\n\r\n|\n\n|\r\r/",$comeback, 2);
		$headers = array();
		$header_data = explode("\n",$comeback[0]);
		$headers['status'] = $header_data[0]; 
		array_shift($header_data); 
		foreach($header_data as $part) {
			$h = explode(":", $part);
			$headers[trim($h[0])] = trim($h[1]);
		}
		print_r(json_decode($comeback[1]));
		return array('headers' => $headers, 'response' => $comeback[1]);
	}
} 
}
$CF_obj = new product_functions($store);
?>