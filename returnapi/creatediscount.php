<?php
   
   include_once '../append/connection.php';
   include_once ABS_PATH . '/user/cls_header.php';

class product_functions extends common_function {
  
    public function __construct($store = '') {
       $url = $_SERVER['REQUEST_URI'];
       $url_components = parse_url($url);
       $query = isset($url_components['query']) ? $url_components['query'] : '';
       parse_str($query, $params);
       $store = (isset($params['store']) && $params['store'] != '' )  ? $params['store'] : "dashboardmanage.myshopify.com";
       $title = (isset($params['title']) && $params['title'] != '' )  ? $params['title'] : $returnArray["title"] = 'Title not found';
       $target_type = (isset($params['target_type']) && $params['target_type'] != '' )  ? $params['target_type'] : $returnArray["target_type"] = 'Target_type not found';
       $target_selection = (isset($params['target_selection']) && $params['target_selection'] != '' )  ? $params['target_selection'] : $returnArray["target_selection"] = 'Target_selection not found';
       $allocation_method = (isset($params['allocation_method']) && $params['allocation_method'] != '' )  ? $params['allocation_method'] : $returnArray["allocation_method"] = 'Allocation_method not found';
       $value_type = (isset($params['value_type']) && $params['value_type'] != '' )  ? $params['value_type'] : $returnArray["value_type"] = 'Value_type not found';
       $value = (isset($params['value']) && $params['value'] != '' )  ? $params['value'] : $returnArray["value"] = 'Value not found';
       $customer_selection = (isset($params['customer_selection']) && $params['customer_selection'] != '' )  ? $params['customer_selection'] : $returnArray["customer_selection"] = 'Customer_selection not found';
       $starts_at = (isset($params['starts_at']) && $params['starts_at'] != '' )  ? $params['starts_at'] : $returnArray["starts_at"] = 'Starts_at not found';
       $code = (isset($params['code'])&& $params['code'] != '' )  ? $params['code'] : '';
        if($store == "clsapps.myshopify.com"){
            $password="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";
        }else if($store == "dashboardmanage.myshopify.com"){
            $password="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"; 
        }else if($store == "cls-rewriter.myshopify.com"){
            $password="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"; 
        }
		$api_key="7ced2ccec20a621d975386679497fffa";
		$shopify_endpoint = "/admin/api/2021-07/price_rules.json";
		$type = "POST";
		if(empty($returnArray)){
    		$query = array("price_rule" =>
                array(
                    "title"=>$title,
                    "target_type"=>$target_type,
                    "target_selection"=> $target_selection,
                    "allocation_method"=>$allocation_method,
                    "value_type"=>$value_type,
                    "value"=>$value,
                    "customer_selection"=>$customer_selection,
                    "starts_at"=>$starts_at
                )
            );
    		$shopify_data_list = $this->clsreturn_api_call($api_key, $password, $store, $shopify_endpoint, $query, $type);
    		$priceruleid = $shopify_data_list['response']->price_rule->id;
    	    $shopify_discount_endpoint = "/admin/api/2021-07/price_rules/$priceruleid/discount_codes.json";
    	    if($code != '' ){
    	        $query_discount = array("discount_code" =>
                    array(
                        "code" => $code
                    )
                );
        	    $shopify_data_list = $this->clsreturn_api_call($api_key, $password, $store, $shopify_discount_endpoint, $query_discount, $type);
    	    }else{
    	        print_r(array("code"=>"code not found"));
    	        die;
    	    }
    	    
		}else{
		    print_r($returnArray);
		    die;
		}
    }   
  
public  function clsreturn_api_call($api_key , $password, $store, $shopify_endpoint, $query = array(),$type = '', $request_headers = array()) {
    $cls_shopify_url = "https://" . $api_key .":". $password ."@". $store.  $shopify_endpoint;
    
     if (!is_array($type) && !is_object($type)) {
        (array)$type;
    }
	if (!is_null($query) && in_array($type,array('GET','DELETE'))) $cls_shopify_url = $cls_shopify_url . "?" . http_build_query(array($query));
	$curl = curl_init($cls_shopify_url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	$request_headers[] = "";
    $request_headers[] ="Content-Security-Policy, https:".$store." https://admin.shopify.com";
	
	if (!is_null($password)) $request_headers[] = "X-Shopify-Access-Token: " . $password;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
	if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
		if (is_array($query)) $query = http_build_query($query);
    		curl_setopt($curl, CURLOPT_POSTFIELDS,$query);
	}   

	$comeback = curl_exec($curl);
	$error_number = curl_errno($curl);
	$error_message = curl_error($curl);
	curl_close($curl);
	if ($error_number) {
		return $error_message;
	} else {
		$comeback = preg_split("/\r\n\r\n|\n\n|\r\r/",$comeback, 2);
		$headers = array();
		$header_data = explode("\n",$comeback[0]);
		$headers['status'] = $header_data[0]; 
		array_shift($header_data); 
		foreach($header_data as $part) {
			$h = explode(":", $part,2);
			$headers[trim($h[0])] = trim($h[1]);
		}
		
		print_r($comeback[1]);
		return array('headers' => $headers, 'response' => json_decode($comeback[1]));
	}
} 

}
$CF_obj = new product_functions($store);
?>