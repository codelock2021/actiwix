<?php
   include_once '../append/connection.php';
   include_once ABS_PATH . '/user/cls_header.php';
//    include "../user/cls_header.php";
// $store = isset($_GET['store']) ? $_GET['store'] : '';

class product_functions extends common_function {
  
    public function __construct($store = '') {
        $url = $_SERVER['REQUEST_URI'];
       $url_components = parse_url($url);
       $path = explode("/",$url_components['path']);
       $shopify_api = $path[3];
    
       parse_str($url_components['query'], $params);
       $store = (isset($params['store']) &&  $params['store'] != '') ? $params['store'] : "dashboardmanage.myshopify.com";
       
        if($store == "clsapps.myshopify.com"){
            $password="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";
        }else if($store == "dashboardmanage.myshopify.com"){
            $password="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"; 
        }else if($store == "cls-rewriter.myshopify.com"){
            $password="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"; 
        }else if($store == "managedashboard.myshopify.com"){
            $password="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"; 
        }
		$api_key="7ced2ccec20a621d975386679497fffa";
		$shopify_endpoint = "/admin/api/2021-07/products.json";
		$type = "GET";
		$query = "";
		$shopify_data_list = $this->clsreturn_api_call($api_key, $password, $store, $shopify_endpoint, $query, $type);
    }   
  
public  function clsreturn_api_call($api_key , $password, $store, $shopify_endpoint, $query = array(),$type = '', $request_headers = array()) {
    $cls_shopify_url = "https://" . $api_key .":". $password ."@". $store.  $shopify_endpoint;
     if (!is_array($type) && !is_object($type)) {
        (array)$type;
    }
	if (!is_null($query) && in_array($type,array('GET','DELETE'))) $cls_shopify_url = $cls_shopify_url . "?" . http_build_query(array($query));
	$curl = curl_init($cls_shopify_url);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
	$request_headers[] = "";
        $request_headers[] ="Content-Security-Policy, https:".$store." https://admin.shopify.com";
	
	if (!is_null($password)) $request_headers[] = "X-Shopify-Access-Token: " . $password;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
	if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
		if (is_array($query)) $query = http_build_query($query);
    		curl_setopt($curl, CURLOPT_POSTFIELDS,$query);
	}   

	$comeback = curl_exec($curl);
	$error_number = curl_errno($curl);
	$error_message = curl_error($curl);
	curl_close($curl);
	if ($error_number) {
		return $error_message;
	} else {
		$comeback = preg_split("/\r\n\r\n|\n\n|\r\r/",$comeback, 2);
		$headers = array();
		$header_data = explode("\n",$comeback[0]);
		$headers['status'] = $header_data[0]; 
		array_shift($header_data); 
		foreach($header_data as $part) {
			$h = explode(":", $part,2);
			$headers[trim($h[0])] = trim($h[1]);
		}
		echo "<pre>";
		print_r(json_decode($comeback[1]));die;
		return array('headers' => $headers, 'response' => $comeback[1]);
	}
} 

public function shopify_curl_get($request, $fields = '', $endpoints = '', $limit = 50, $no_pagination = false) {
	$merged = array();
	$page_info = '';
	$last_page = false;
	$limit = '?limit=' . $limit;
	$debug = 0;

	while(!$last_page) {
		$url = $request . $limit . $fields . $endpoints . $page_info;
                echo "<pre>";
                print_r($url);
                echo "</pre>";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers) {
		    $len = strlen($header);
		    $header = explode(':', $header, 2);
		    if (count($header) >= 2) {
		        $headers[strtolower(trim($header[0]))] = trim($header[1]);
		    }
		    return $len;
		});
                echo "<pre>";
                print_r($headers);
                echo "</pre>";
                die;
		$result = curl_exec($curl);
                echo "<pre>";
                print_r($result);
                echo "</pre>";
                die;
		curl_close($curl);

		if(isset($headers['link'])) {
			$links = explode(',', $headers['link']);

			foreach($links as $link) {
				$next_page = false;
				if(strpos($link, 'rel="next"')) {
					$next_page = $link;
				}
			}

			if($next_page) {
				preg_match('~<(.*?)>~', $next_page, $next);
				$url_components = parse_url($next[1]);
				parse_str($url_components['query'], $params);
				$page_info = '&page_info=' . $params['page_info'];
				$endpoints = ''; // Link pagination does not support endpoints on pages 2 and up
			} else {
				$last_page = true; // There's no next page, we're at the last page
			}

		} else {
			$last_page = true; // Couldn't find parameter link in headers, stop loop
		}
		$source_array = json_decode($result, true);
		$merged = array_merge_recursive($merged, $source_array);

		if($no_pagination) {
			$last_page = true;
		}

		// Used for debugging to prevent infinite loops, comment to disable
		//if($debug >= 150) {
		//	//break;
		//}
		//$debug++;

		sleep(1); // Limit calls to 1 per second
	}

	return $merged;
}

}
$CF_obj = new product_functions($store);
?>